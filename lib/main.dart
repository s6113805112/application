import 'package:flutter/material.dart';
import 'push.dart';
import 'loginPage.dart';

void main() {
  runApp(MaterialApp(
    title: 'Navigation Basics',
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*2*/
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'Komon..Transport',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  'ขนย้ายบ้าน ย้ายสิ่งของ ',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),

          /*3*/
        ],
      ),
    );
    return MaterialApp(
      title: 'KOMONTRANSPORT',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFFFFC107),
          title: Text('KOMONTRANSPORT'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/Untitled-8.png',
              width: 700,
              height: 258,
              fit: BoxFit.cover,
            ),
            titleSection,
            iconList,
            Container(
              width: 300,
              height: 80,
              child: ElevatedButton(
                child: Text('จองรถขนของ'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LoginPage()),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

final iconList = DefaultTextStyle.merge(
  child: Container(
    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            Icon(Icons.perm_identity, color: Colors.red[800], size: 70),
            Text('โกมนขนส่ง'),
          ],
        ),
        Column(
          children: [
            Icon(Icons.tablet_mac_outlined,
                color: Colors.yellow[500], size: 50),
            Text('081-1348801'),
          ],
        ),
        Column(
          children: [
            Icon(
              Icons.time_to_leave_rounded,
              color: Colors.green,
              size: 50,
            ),
            Text('รับขนย้ายสิ่งของทั่วไทย'),
          ],
        ),
      ],
    ),
  ),
);
